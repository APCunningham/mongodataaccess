package com.gmx.cunningham.ap.DataAccess.Utils;

import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.FieldEnd;
import org.mongodb.morphia.query.Query;

import java.util.*;

/**
 * @author Andrew Cunningham
 * @apc.created 9/08/15
 */
public class DBQuery {
    public enum Relation{
        AND, OR
    }

    private DBQuery left = null;
    private Relation relation = null;
    private DBQuery right = null;
    private DBCondition condition = null;
    private List<DBOrder> order = new LinkedList<>();
    private int limit = -1;
    private int offset = -1;

    private DBQuery(){}

    public static DBQuery andList(Queue<DBQuery> que){
        if(que.size() == 1) return que.remove();
        return DBQuery.and(que.remove(), andList(que));
    }

    public static DBQuery andList(DBQuery... cond){
        Queue<DBQuery> q = new LinkedList<>();
        Collections.addAll(q, cond);
        return andList(q);
    }

    private static DBQuery and(DBQuery left, DBQuery right){
        DBQuery dbq = new DBQuery();
        dbq.left = left;
        dbq.right = right;
        dbq.relation = Relation.AND;
        return dbq;
    }

    public static DBQuery orList(Queue<DBQuery> que){
        if(que.size() == 1) return que.remove();
        return DBQuery.or(que.remove(), orList(que));
    }

    public static DBQuery orList(DBQuery... cond){
        Queue<DBQuery> q = new LinkedList<>();
        Collections.addAll(q, cond);
        return orList(q);
    }

    private static DBQuery or(DBQuery left, DBQuery right){
        DBQuery dbq = new DBQuery();
        dbq.left = left;
        dbq.right = right;
        dbq.relation = Relation.OR;
        return dbq;
    }

    public static DBQuery condition(String field, DBCondition.Relation rel, Object value){
        DBQuery dbq = new DBQuery();
        dbq.condition = new DBCondition(field, rel, value);
        return dbq;
    }

    public static <E extends Enum<E>> DBQuery condition(E field, DBCondition.Relation rel, Object value){
        DBQuery dbq = new DBQuery();
        dbq.condition = new DBCondition(field.name(), rel, value);
        return dbq;
    }

    public DBQuery order(String field, DBOrder.Ordering ordering){
        this.order.add(new DBOrder(field, ordering));
        return this;
    }

    public DBQuery limit(int num){
        this.limit = num;
        return this;
    }

    public DBQuery offset(int num){
        this.offset = num;
        return this;
    }

    public DBQuery getLeft() {
        return left;
    }

    public Relation getRelation() {
        return relation;
    }

    public DBQuery getRight() {
        return right;
    }

    public DBCondition getCondition() {
        return condition;
    }

    public List<DBOrder> getOrder() {
        return order;
    }

    public int getLimit() {
        return limit;
    }

    public int getOffset() {
        return offset;
    }

    public static Criteria criteria(Query q, DBQuery query){
        if(query.getCondition()!=null){
            FieldEnd c = q.criteria(query.getCondition().getField());
            Object o = query.getCondition().getValue();

            switch (query.getCondition().getOperator()) {
                case CONTAINS:
                    return (Criteria) c.containsIgnoreCase((String) o);
                case STARTS:
                    return (Criteria) c.startsWithIgnoreCase((String) o);
                case ENDS:
                    return (Criteria) c.endsWithIgnoreCase((String) o);
                case CONTAINSMATCHCASE:
                    return (Criteria) c.containsIgnoreCase((String) o);
                case STARTSMATCHCASE:
                    return (Criteria) c.startsWithIgnoreCase((String) o);
                case ENDSMATCHCASE:
                    return (Criteria) c.endsWithIgnoreCase((String) o);
                case EQ:
                    return (Criteria)c.equal(o);
                case LT:
                    return (Criteria)c.lessThan(o);
                case GT:
                    return (Criteria)c.greaterThan(o);
                case LTE:
                    return (Criteria)c.lessThanOrEq(o);
                case GTE:
                    return (Criteria)c.greaterThanOrEq(o);
                case NE:
                    return (Criteria)c.notEqual(o);
                case NONEOF:
                    return (Criteria)c.hasNoneOf((Iterable) o);
                case IN:
                    return (Criteria)c.in((Iterable) o);
                default:
                    throw new RuntimeException("unknown operator");
            }
        }

        if(query.getRelation() == DBQuery.Relation.OR){
            return q.or(criteria(q, query.getLeft()), criteria(q, query.getRight()));
        }

        return q.and(criteria(q, query.getLeft()), criteria(q, query.getRight()));
    }
}
