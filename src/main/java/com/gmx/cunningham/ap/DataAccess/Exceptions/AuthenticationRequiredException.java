package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 8/08/15
 */
public class AuthenticationRequiredException extends FieldException {

    public AuthenticationRequiredException(String fieldName){
        super("Authentication Required", fieldName);
    }
}
