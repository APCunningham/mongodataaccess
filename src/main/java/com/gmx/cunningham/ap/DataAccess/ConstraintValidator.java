package com.gmx.cunningham.ap.DataAccess;

import com.gmx.cunningham.ap.DataAccess.Annotations.Unique;
import com.gmx.cunningham.ap.DataAccess.Utils.Error;
import com.gmx.cunningham.ap.DataAccess.Utils.Errors;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.logging.Logger;
import org.mongodb.morphia.logging.jdk.JDKLoggerFactory;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;

import static com.gmx.cunningham.ap.DataAccess.Utils.ReflectionUtils.*;


/**
 * @author Andrew Cunningham
 * @apc.created 22/07/15
 */
public class ConstraintValidator {
    private static Logger log = new JDKLoggerFactory().get(ConstraintValidator.class);

    private Validator validator;
    private DB DB;

    public ConstraintValidator(DB db){
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
        DB = db;
    }

    public Errors validate(Object obj){
        Class<?> clazz = obj.getClass();
        Errors vs = new Errors();

        for(Field f : clazz.getDeclaredFields()){
            Errors fErrors = new Errors(validator.validateProperty(obj, f.getName()));
            vs.addErrors(fErrors);
            if(fErrors.hasErrors()) continue;
            if(f.isAnnotationPresent(Unique.class)){
                Error v = validateUnique(obj, f);
                if(v != null) vs.addErrors(v);
            } else if(f.isAnnotationPresent(Reference.class)){
                Error v = validateReference(obj, f);
                if(v != null) vs.addErrors(v);
            }
        }

        return vs;
    }

    public Errors validate(Object obj, String... fields){
        Errors vs = new Errors();

        for(String sf : fields){
            Field f = getField(obj, sf);
            Errors fErrors = new Errors(validator.validateProperty(obj, f.getName()));
            vs.addErrors(fErrors);
            if(fErrors.hasErrors()) continue;
            if(f.isAnnotationPresent(Unique.class)){
                Error v = validateUnique(obj, f);
                if(v != null) vs.addErrors(v);
            } else if(f.isAnnotationPresent(Reference.class)){
                Error v = validateReference(obj, f);
                if(v != null) vs.addErrors(v);
            }
        }

        return vs;
    }

    private Error validateUnique(Object obj, Field f){
        Class<?> clazz = obj.getClass();
        try {
            if(DB.existClassKey(clazz, f.getName(), getProp(obj, f))){
                return new Error(f, "Duplicate entry on unique field");
            }
        } catch (RuntimeException e) {
            return new Error(f, e.getMessage());
        }

        return null;
    }

    private <T> Error validateReference(T obj, Field f){

        try {
            if(Collection.class.isAssignableFrom(f.getType())){
                for(Object o: (Collection)getProp(obj, f)){
                    if (!DB.existClassKey(o.getClass(), getId(o))) {
                        return new Error(f, "Reference does not exist");
                    }
                }
            } else if(f.getType().isArray()){
                for(Object o: (Object[])getProp(obj, f)){
                    if (!DB.existClassKey(o.getClass(), getId(o))) {
                        return new Error(f, "Reference does not exist");
                    }
                }
            } else {
                if(getProp(obj, f)==null) return null;
                Class<?> clazz = f.getType();
                if (!DB.existClassKey(clazz, getId(getProp(obj, f)))) {
                    return new Error(f, "Reference does not exist");
                }
            }
        } catch (RuntimeException e) {
            log.info(e.getMessage());
            return new Error(f, e.getMessage());
        }

        return null;
    }
}
