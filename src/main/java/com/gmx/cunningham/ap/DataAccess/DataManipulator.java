package com.gmx.cunningham.ap.DataAccess;

import com.gmx.cunningham.ap.DataAccess.Annotations.*;
import com.gmx.cunningham.ap.DataAccess.Exceptions.AuthenticationRequiredException;
import com.gmx.cunningham.ap.DataAccess.Utils.Credentials;
import com.gmx.cunningham.ap.DataAccess.Utils.IdGenerator;
import com.gmx.cunningham.ap.DataAccess.Utils.ReflectionUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.reflect.Field;

import static com.gmx.cunningham.ap.DataAccess.Utils.ReflectionUtils.*;


/**
 * @author Andrew Cunningham
 * @apc.created 29/07/15
 */
public class DataManipulator {
    private DB DB;

    public DataManipulator(DB db){
        DB = db;
    }

    public <T> T onResponse(T obj){
        if(obj == null) return null;
        if(obj.getClass().isAnnotationPresent(Private.class)){
            String[] privFields = obj.getClass().getDeclaredAnnotation(Private.class).privFields();
            for(String s : privFields){
                setProp(obj, s, "");
            }
        }
        return obj;
    }

    public <T> T onRequest(T obj){
        for(Field f : obj.getClass().getDeclaredFields()){
            try {
                if (f.isAnnotationPresent(Encrypt.class)) {
                    PasswordEncoder pe = f.getDeclaredAnnotation(Encrypt.class).encoder().newInstance();
                    setProp(obj, f, pe.encode((String) ReflectionUtils.getProp(obj, f)));
                } else if (f.isAnnotationPresent(AuthRequired.class) && DB.get(obj) != null) {
                    throw new AuthenticationRequiredException(f.getName());
                }
            } catch (InstantiationException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return obj;
    }

    public <T> T onRequest(T obj, Credentials credentials){
        for(Field f : obj.getClass().getDeclaredFields()){
            if(f.isAnnotationPresent(Encrypt.class)){
                try {
                    PasswordEncoder pe = f.getDeclaredAnnotation(Encrypt.class).encoder().newInstance();
                    setProp(obj, f, pe.encode((String) ReflectionUtils.getProp(obj, f)));
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            } else if(f.isAnnotationPresent(AuthRequired.class)){
                if(!DB.authenticate(credentials)) return null;
            }
        }
        return obj;
    }

    @SuppressWarnings("unchecked")
    public <T> T genId(T obj){
        for(Field f : obj.getClass().getDeclaredFields()) {
            if (f.isAnnotationPresent(GenId.class)) {
                try {
                    GenId gi = f.getAnnotation(GenId.class);

                    if (getProp(obj, f) != null) return obj;

                    Class refClass = gi.refClass();
                    if (refClass == GenId.class) {
                        refClass = obj.getClass();
                    }

                    String refField = gi.refField();
                    if (refField.isEmpty()) {
                        refField = f.getName();
                    }

                    IdGenerator idGen = gi.generator().newInstance();
                    idGen.setDb(DB)
                            .setRegex(gi.regex())
                            .setRefClass(refClass)
                            .setRefField(refField);


                    setProp(obj, f, idGen.generate());

                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            } else if(f.isAnnotationPresent(IdHash.class)){
                IdHash ih = f.getAnnotation(IdHash.class);
                Object id = getProp(obj, ih.field());
                setProp(obj, f, id.hashCode());
            }
        }
        return obj;
    }
}
