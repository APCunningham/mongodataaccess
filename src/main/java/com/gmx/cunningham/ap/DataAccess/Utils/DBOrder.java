package com.gmx.cunningham.ap.DataAccess.Utils;

import org.apache.logging.log4j.core.config.Order;

/**
 * @author Andrew Cunningham
 * @apc.created 9/08/15
 */
public class DBOrder {
    public enum Ordering{
        ASC(""), DESC("-");

        private String value;

        Ordering(String value){
            this.value = value;
        }

        public String val(){
            return value;
        }
    }

    private String field;
    private Ordering ordering;

    public DBOrder(String field, Ordering ordering) {
        this.field = field;
        this.ordering = ordering;
    }

    public String getField() {
        return field;
    }

    public DBOrder setField(String field) {
        this.field = field;
        return this;
    }

    public Ordering getOrdering() {
        return ordering;
    }

    public DBOrder setOrdering(Ordering ordering) {
        this.ordering = ordering;
        return this;
    }
}
