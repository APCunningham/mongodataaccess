package com.gmx.cunningham.ap.DataAccess.Utils;

import com.gmx.cunningham.ap.DataAccess.DB;
import com.mifmif.common.regex.Generex;

/**
 * @author Andrew Cunningham
 * @apc.created 20/07/15
 */
@SuppressWarnings("StatementWithEmptyBody")
public class IdGenerator {
    private Generex gen;
    private Class refClass;
    private String refField = null;
    private DB db;

    public IdGenerator setRegex(String regex) {
        this.gen = new Generex(regex);
        return this;
    }

    public IdGenerator setRefClass(Class refClass) {
        this.refClass = refClass;
        return this;
    }

    public IdGenerator setRefField(String refField) {
        this.refField = refField;
        return this;
    }

    public IdGenerator setDb(DB db) {
        this.db = db;
        return this;
    }

    public String generate(){
        String id;
        if(refField==null){
            while(db.existClassKey(refClass, id = gen.random()));
        } else {
            while(db.existClassKey(refClass, refField, id = gen.random()));
        }
        return id;
    }
}
