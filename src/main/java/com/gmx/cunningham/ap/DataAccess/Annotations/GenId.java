package com.gmx.cunningham.ap.DataAccess.Annotations;

import com.gmx.cunningham.ap.DataAccess.Utils.IdGenerator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Andrew Cunningham
 * @apc.created 30/07/15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface GenId {
    Class<? extends IdGenerator> generator() default IdGenerator.class;
    String regex();
    Class refClass() default GenId.class;
    String refField() default "";
}
