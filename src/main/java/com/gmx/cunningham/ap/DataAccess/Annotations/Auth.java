package com.gmx.cunningham.ap.DataAccess.Annotations;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Andrew Cunningham
 * @apc.created 1/08/15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Auth {
    String usernameField() default "username";
    String passwordField() default "password";
    Class<? extends PasswordEncoder> encoder() default BCryptPasswordEncoder.class;
}
