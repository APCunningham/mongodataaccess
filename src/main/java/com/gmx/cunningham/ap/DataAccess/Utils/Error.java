package com.gmx.cunningham.ap.DataAccess.Utils;

import java.lang.reflect.Field;

/**
 * @author Andrew Cunningham
 * @apc.created 22/07/15
 */
public class Error {
    private String field;
    private String message;

    public Error(Field field, String message) {
        this.field = field.getName();
        this.message = message;
    }

    public Error(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public Error setField(String field) {
        this.field = field;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Error setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString(){
        return field+": "+message;
    }
}
