package com.gmx.cunningham.ap.DataAccess.Utils;

import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.FieldEnd;
import org.mongodb.morphia.query.Query;

/**
 * @author Andrew Cunningham
 * @apc.created 9/08/15
 */
public class DBCondition {
    public enum Relation{
        EQ,
        GT,
        LT,
        GTE,
        LTE,
        NE,
        CONTAINS,
        STARTS,
        ENDS,
        NONEOF,
        IN,
        CONTAINSMATCHCASE,
        STARTSMATCHCASE,
        ENDSMATCHCASE
    }

    private String field;
    private Relation operator;
    private Object value;

    public DBCondition(String field, Relation operator, Object value) {
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public DBCondition setField(String field) {
        this.field = field;
        return this;
    }

    public Relation getOperator() {
        return operator;
    }

    public DBCondition setOperator(Relation operator) {
        this.operator = operator;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public DBCondition setValue(Object value) {
        this.value = value;
        return this;
    }
}
