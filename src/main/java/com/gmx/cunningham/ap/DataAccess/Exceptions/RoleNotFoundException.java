package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 20/07/15
 */
public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException(String name){
        super("Role not found: "+name);
    }
}
