package com.gmx.cunningham.ap.DataAccess;

import com.mongodb.MongoClient;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Andrew Cunningham
 * @apc.created 1/08/15
 */
public class DBFactory {
    private Set<String> packages = new HashSet<>();
    private MongoClient mongoClient = null;
    private String dbName = null;
    private Class authClass = null;

    public DBFactory addPackages(String... packages) {
        Collections.addAll(this.packages, packages);
        return this;
    }

    public DBFactory setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
        return this;
    }

    public DBFactory setDbName(String dbName) {
        this.dbName = dbName;
        return this;
    }

    public DBFactory setAuthClass(Class authClass) {
        this.authClass = authClass;
        return this;
    }

    public DB build(){
        if(mongoClient == null) throw new RuntimeException("No mongo client in DBFactory");
        if(dbName == null) throw new RuntimeException("No DataBase name");
        return new DB(mongoClient, dbName, packages, authClass);
    }
}
