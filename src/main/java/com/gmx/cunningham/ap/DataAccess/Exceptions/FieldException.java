package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 8/08/15
 */
public class FieldException extends RuntimeException{
    private String fieldName;

    public FieldException(String msg, String fieldName) {
        super(msg);
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
