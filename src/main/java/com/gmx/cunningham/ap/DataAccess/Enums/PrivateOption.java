package com.gmx.cunningham.ap.DataAccess.Enums;

/**
 * @author Andrew Cunningham
 * @apc.created 31/07/15
 */
public enum PrivateOption {
    HIDE(true), SHOW(false);

    boolean val;

    PrivateOption(boolean val){
        this.val = val;
    }

    public boolean val(){
        return val;
    }
}
