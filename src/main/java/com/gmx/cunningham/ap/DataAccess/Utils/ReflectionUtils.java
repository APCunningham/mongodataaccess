package com.gmx.cunningham.ap.DataAccess.Utils;

import com.gmx.cunningham.ap.DataAccess.Annotations.IdHash;
import com.gmx.cunningham.ap.DataAccess.Annotations.Unique;
import org.mongodb.morphia.annotations.Id;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Andrew Cunningham
 * @apc.created 29/07/15
 */
public class ReflectionUtils {
    public static <T> Object getId(T obj){
        return getProp(obj, getIdField(obj));
    }

    public static <T, K> Object setId(T obj, K val){
        return setProp(obj, getIdField(obj), val);
    }

    public static <T> Field getIdField(T obj){
        Class<?> clazz = obj.getClass();
        for(Field f : clazz.getDeclaredFields()){
            if(f.isAnnotationPresent(Id.class)) return f;
        }
        throw new RuntimeException("No Id found");
    }

    public static <T> Field[] getUniqueFields(T obj){
        Class<?> clazz = obj.getClass();
        List<Field> fields = new LinkedList<>();
        for(Field f : clazz.getDeclaredFields()){
            if(f.isAnnotationPresent(Unique.class)) fields.add(f);
        }
        return (Field[])fields.toArray();
    }

    public static <T> Object getProp(T obj, Field f) {
        try {
            if(f.isAnnotationPresent(IdHash.class))
                return getProp(obj, f.getAnnotation(IdHash.class).field()).hashCode();
            f.setAccessible(true);
            return f.get(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Object getProp(T obj, String f) {
        try {
            Field field = obj.getClass().getDeclaredField(f);
            return getProp(obj, field);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T, V> T setProp(T obj, String f, V value) {
        try {
            Field field = obj.getClass().getDeclaredField(f);
            field.setAccessible(true);
            field.set(obj, value);
            return obj;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T, V> T setProp(T obj, Field f, V value) {
        try {
            f.setAccessible(true);
            f.set(obj, value);
            return obj;
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Field getField(T obj, String f){
        try{
            return obj.getClass().getDeclaredField(f);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> Class<T> getClazz(T obj){
        return (Class<T>)obj.getClass();
    }

    public static boolean isPrimitive(Class clz){
        return clz.isPrimitive()
               || clz == Integer.class || clz == Double.class || clz == Float.class
               || clz == Long.class    || clz == String.class || clz == Boolean.class
               || clz == Byte.class    || clz == Short.class  || clz == Character.class;
    }
}
