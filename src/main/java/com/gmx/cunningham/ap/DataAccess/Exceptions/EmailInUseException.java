package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 20/07/15
 */
public class EmailInUseException extends RuntimeException {
    public EmailInUseException(String email){
        super("Email already registered: "+email);
    }
}
