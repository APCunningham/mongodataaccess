package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 8/08/15
 */
public class UserNotFoundException extends FieldException {

    public UserNotFoundException(String fieldName) {
        super("User Not found", fieldName);
    }
}