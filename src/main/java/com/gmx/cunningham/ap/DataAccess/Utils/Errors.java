package com.gmx.cunningham.ap.DataAccess.Utils;

import javax.validation.ConstraintViolation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Andrew Cunningham
 * @apc.created 22/07/15
 */
public class Errors {
    private Set<Error> errors;

    public Errors(){
        errors = new HashSet<>();
    }

    public Errors(Set<ConstraintViolation<Object>> errors){
        this();
        for(ConstraintViolation<Object> cvo : errors){
            this.errors.add(new Error(cvo.getPropertyPath().toString(), cvo.getMessage()));
        }
    }

    public Errors addErrors(Error... violation){
        Collections.addAll(errors, violation);
        return this;
    }

    public Errors addErrors(Errors errors){
        this.errors.addAll(errors.errors);
        return this;
    }

    public Set<Error> getErrors() {
        return errors;
    }

    public boolean hasErrors(){
        return !errors.isEmpty();
    }

    public int size(){
        return errors.size();
    }

    public Errors removeErrorByField(String... field){
        for(Error v : errors){
            for(String f : field){
                if(f.equals(v.getField())) errors.remove(v);
            }
        }
        return this;
    }

    public Errors removeErrorByField(Field... field){
        for(Error v : errors){
            for(Field f : field){
                if(f.getName().equals(v.getField())) errors.remove(v);
            }
        }
        return this;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Error v : errors){
            sb.append(v.toString()).append("\n");
        }
        return sb.toString();
    }

    public Errors copy(){
        Errors vs = new Errors();
        vs.errors.addAll(this.errors);
        return vs;
    }
}
