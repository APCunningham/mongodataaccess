package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 20/07/15
 */
public class NoIdRegExException extends RuntimeException {
    public NoIdRegExException(String name){
        super("No "+name+" regular expression");
    }
}
