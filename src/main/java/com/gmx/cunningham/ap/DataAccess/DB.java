package com.gmx.cunningham.ap.DataAccess;

import com.gmx.cunningham.ap.DataAccess.Annotations.Auth;
import com.gmx.cunningham.ap.DataAccess.Enums.PrivateOption;
import com.gmx.cunningham.ap.DataAccess.Exceptions.AuthenticationFailedException;
import com.gmx.cunningham.ap.DataAccess.Exceptions.AuthenticationRequiredException;
import com.gmx.cunningham.ap.DataAccess.Exceptions.UserNotFoundException;
import com.gmx.cunningham.ap.DataAccess.Utils.*;
import com.gmx.cunningham.ap.DataAccess.Utils.Error;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.FieldEnd;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.*;

import static com.gmx.cunningham.ap.DataAccess.Utils.ReflectionUtils.*;

/**
 * @author Andrew Cunningham
 * @apc.created 20/07/15
 */
public class DB {
    private final Morphia morphia = new Morphia();
    private Datastore datastore;
    private DataManipulator dataManipulator;
    private ConstraintValidator constraintValidator;
    private Class authClass;

    protected DB(MongoClient client, String dbName, Set<String> packages){
        this(client, dbName, packages, null);
    }

    protected DB(MongoClient client, String dbName, Set<String> packages, Class authClass){
        for(String p : packages) morphia.mapPackage(p);
        datastore = morphia.createDatastore(client, dbName);
        datastore.ensureIndexes();

        dataManipulator = new DataManipulator(this);
        constraintValidator = new ConstraintValidator(this);

        this.authClass = authClass;
    }

    protected <T> Errors interceptIn(T t){
        Errors errors;
        dataManipulator.genId(t);
        if((errors = validate(t)).hasErrors()) return errors;
        try {
            dataManipulator.onRequest(t);
        } catch (AuthenticationRequiredException e){
            return errors.addErrors(new Error(e.getFieldName(), e.getMessage()));
        }
        return errors;
    }

    protected <T> Errors interceptIn(T t, Credentials credentials){
        Errors errors;
        dataManipulator.genId(t);
        if((errors = validate(t)).hasErrors()) return errors;
        try {
            dataManipulator.onRequest(t, credentials);
        } catch (AuthenticationFailedException | UserNotFoundException e){
            return errors.addErrors(new Error(e.getFieldName(), e.getMessage()));
        }
        return errors;
    }

    protected <T> T interceptOut(T t, PrivateOption priv){
        if(priv.val()) privatise(t);
        return t;
    }

    protected <T> List<T> interceptOut(List<T> t, PrivateOption priv){
        if(priv.val()) privatise(t);
        return t;
    }

    protected Errors validate(Object obj){
        return constraintValidator.validate(obj);
    }

    protected Errors validate(Object obj, String... fields){
        return constraintValidator.validate(obj, fields);
    }

    protected <T> T privatise(T obj){
        return dataManipulator.onResponse(obj);
    }

    protected <T> List<T> privatise(List<T> obj){
        for(T t : obj){
            dataManipulator.onResponse(t);
        }
        return obj;
    }

    public <T> void delete(final T model){
        datastore.delete(getClazz(model), getId(model));
    }

    public <T> void delete(final T model, Credentials credentials){
        interceptIn(model, credentials);
        datastore.delete(getClazz(model), getId(model));
    }

    public <T> void deleteAll(final T model, String field){
        List<T> list = getList(model, field);
        for(T t : list) {
            datastore.delete(t);
        }
    }

    public <T> boolean exist(final T model, String... fieldNames){
        Queue<DBQuery> fields = new LinkedList<>();
        for(String s : fieldNames) fields.add(DBQuery.condition(s, DBCondition.Relation.EQ, getProp(model, s)));
        if(fields.size()==0) fields.add(DBQuery.condition(getIdField(model).getName(), DBCondition.Relation.EQ, getId(model)));
        return find(model.getClass(), DBQuery.andList(fields)) != null;
    }

    public <T> boolean exist(final T model){
        return get(model) != null;
    }

    public <T, K> boolean existClassKey(final Class<T> model, String fieldName, final K key){
        return datastore.find(model, fieldName, key).get() != null;
    }

    public <T, K> boolean existClassKey(final Class<T> model, final K key){
        return datastore.get(model, key) != null;
    }

    public <T> T find(final Class<T> model, DBQuery query){
        return find(model, query, PrivateOption.HIDE);
    }

    public <T> T find(final Class<T> model, DBQuery query, PrivateOption priv){
        return interceptOut(queryToQuery(model, query).get(), priv);
    }

    public <T> List<T> findList(final Class<T> model, DBQuery query){
        return findList(model, query, PrivateOption.HIDE);
    }

    public <T> List<T> findList(final Class<T> model, DBQuery query, PrivateOption priv){
        return interceptOut(queryToQuery(model, query).asList(), priv);
    }

    public <T> T get(final T obj){
        return get(obj, PrivateOption.HIDE);
    }

    public <T> T get(final T obj, PrivateOption priv){
        if(obj == null) return null;
        return interceptOut(datastore.get(getClazz(obj), getId(obj)), priv);
    }

    public <T> T get(final T obj, String field){
        return get(obj, field, PrivateOption.HIDE);
    }

    public <T> T get(final T obj, String field, PrivateOption priv){
        if(obj == null) return null;
        return interceptOut(datastore.find(getClazz(obj), field, getProp(obj, field)).get(), priv);
    }

    public <T> List<T> getAll(final Class<T> clz){
        return getAll(clz, PrivateOption.HIDE);
    }

    public <T> List<T> getAll(final Class<T> clz, PrivateOption option){
        return interceptOut(datastore.find(clz).asList(), option);
    }

    public <T> List<T> getList(T model, String field){
        return getList(model, field, PrivateOption.HIDE);
    }

    public <T> List<T> getList(T model, String field, PrivateOption priv){
        if(model == null) return new LinkedList<>();
        return interceptOut(datastore.find(getClazz(model), field, getProp(model, field)).asList(), priv);
    }

    public <T> T match(T model){
        return match(model, PrivateOption.HIDE);
    }

    public <T> T match(T model, PrivateOption priv){
        Queue<DBQuery> cond = getConditions(model, "");
        if(cond.size()==0) return null;
        return find(getClazz(model), DBQuery.andList(cond));
    }

    public <T> List<T> matchList(T model){
        return matchList(model, PrivateOption.HIDE);
    }

    public <T> List<T> matchList(T model, PrivateOption priv){
        Queue<DBQuery> cond = getConditions(model, "");
        if(cond.size()==0) return getAll(getClazz(model));
        return findList(getClazz(model), DBQuery.andList(cond));
    }

    public <T> Errors save(T obj){
        Errors errors = new Errors();
        if(exist(obj)){
            return errors.addErrors(new Error(getIdField(obj), "duplicate id"));
        }
        if((errors = interceptIn(obj)).hasErrors()) return errors;
        datastore.save(obj);
        return errors;
    }

    private <T> Errors save(T obj, Credentials credentials){
        Errors errors = new Errors();
        if(exist(obj)){
            return errors.addErrors(new Error(getIdField(obj), "duplicate id"));
        }
        if((errors = interceptIn(obj, credentials)).hasErrors()) return errors;
        datastore.save(obj);
        return errors;
    }

    public <T> Errors update(T obj, String... fields){
        Errors errors;
        if(exist(obj)) return new Errors().addErrors(new Error(getIdField(obj), "does not exist"));
        if((errors = validate(obj, fields)).hasErrors()) return errors;
        UpdateOperations<T> update = datastore.createUpdateOperations(getClazz(obj));
        for(String f : fields){
            update.set(f, getProp(obj, f));
        }
        datastore.update(obj, update);
        return errors;
    }

    public <T> Errors update(T obj, Credentials credentials, String... fields){
        Errors errors;
        if(exist(obj)) return new Errors().addErrors(new Error(getIdField(obj), "does not exist"));
        if((errors = validate(obj, fields)).hasErrors()) return errors;
        UpdateOperations<T> update = datastore.createUpdateOperations(getClazz(obj));
        for(String f : fields){
            update.set(f, getProp(obj, f));
        }
        datastore.update(obj, update);
        return errors;
    }

    public boolean authenticate(Credentials credentials){
        try {
            String usernameField = "username";
            String passwordField = "password";
            Class<? extends PasswordEncoder> encoder = BCryptPasswordEncoder.class;
            if(authClass.isAnnotationPresent(Auth.class)) {
                usernameField = ((Auth) authClass.getDeclaredAnnotation(Auth.class)).usernameField();
                passwordField = ((Auth) authClass.getDeclaredAnnotation(Auth.class)).passwordField();
                encoder = ((Auth) authClass.getDeclaredAnnotation(Auth.class)).encoder();
            }
            Object o = authClass.newInstance();
            setProp(o, usernameField, credentials.getUsername());
            o = get(o, usernameField, PrivateOption.SHOW);
            if(o==null) return false;
            PasswordEncoder pe = encoder.newInstance();
            return pe.matches(credentials.getPassword(), (String) getProp(o, passwordField));
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private <T> Queue<DBQuery> getConditions(T model, String prefix){
        Class<T> clazz = getClazz(model);
        Queue<DBQuery> conditions = new LinkedList<>();
        for(Field f : clazz.getDeclaredFields()){
            if(Collection.class.isAssignableFrom(f.getDeclaringClass())) continue;
            Object o;
            if(!isPrimitive(f.getDeclaringClass()) && (o = getProp(model, f)) != null) {
                conditions.addAll(getConditions(o, (prefix.equals("")?"":prefix+".")+f.getName()));
            } else if((o = getProp(model, f)) != null){
                conditions.add(DBQuery.condition((prefix.equals("")?"":prefix+".")+f.getName(), DBCondition.Relation.EQ, o));
            }
        }

        return conditions;
    }

    private <T> Query<T> queryToQuery(Class<T> clazz, DBQuery query){
        Query<T> q = datastore.createQuery(clazz);
        DBQuery.criteria(q, query);
        for(DBOrder dbo : query.getOrder()) q.order(dbo.getOrdering().val()+dbo.getField());
        if(query.getLimit() != -1) q.limit(query.getLimit());
        if(query.getOffset() != -1) q.offset(query.getOffset());
        return q;
    }

}
