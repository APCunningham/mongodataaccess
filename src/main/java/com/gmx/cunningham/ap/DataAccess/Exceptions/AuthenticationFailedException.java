package com.gmx.cunningham.ap.DataAccess.Exceptions;

/**
 * @author Andrew Cunningham
 * @apc.created 8/08/15
 */
public class AuthenticationFailedException extends FieldException{

    public AuthenticationFailedException(String fieldName){
        super("Authentication failed", fieldName);
    }
}
