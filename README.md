# Description #

This repo is used to simplify an already simplified package.
It simplifies Morphia which simplifies communication with a MongoDB.

## Installation ##

To use this you must install with maven or compile to jar and include.
`mvn clean install`
after install add to maven
```
<dependency>
    <groupId>com.gmx.cunningham.ap</groupId>
    <artifactId>MongoDataAccess</artifactId>
    <version>0.2.16</version>
</dependency>
```

## example ##

```
@Entity("Users")
@Private(privFields = {"password"})
public class User{
    @AuthRequired @NotBlank @Size(min = 8)
    @Encrypt
    private String password;
    @Id @NotBlank @Email
    private String email;
    @Size(min=1) @Reference
    private Set<Role> roles = new HashSet<>();

    // getters and setters
}
```

## Annotations ##

### @Auth ###

Allows developer to define an Authentication the user identifier, secret field
and password encoder in an authentication class.

### `@AuthRequired` ###

Developer can define which fields require additional authentication to update.

### `@Encrypt` ###

Defines a field to be encrypted. Encoder option allows to select an encoder
class that extends spring PasswordEncoder.

### `@GenId` ###

Defines a generated id field. Must specify a regex for generation.

### `@IdHash` ###

Workaround for multi-field id. Must be a static inner class.

### `@Private` ###

Defines secret fields. On get or find the specified fields will return blank, null or -1

### `@Unique` ###

Defines a unique field and allows db to check for clash. Used on not id fields

### Usage ###

## Create connection ##

```
Set<String> packages = new HashSet<>();
Collections.addAll(packages,
        "au.edu.newcastle.G6.FlightPub.usermgmt.Account",
        "au.edu.newcastle.G6.FlightPub.usermgmt.Address",
        "au.edu.newcastle.G6.FlightPub.usermgmt.Role",
        "au.edu.newcastle.G6.FlightPub.usermgmt.User");
DB db = new DB(new MongoClient(), "FlightPub", packages, User.class);
```

`new DB(<<MongoClient>>, <<DB Name>>, <<Entity Classes>>, <<Authentication Class>>);`

Mongo client can be any configuration that the mongo driver allows.

## Save ##

`db.save(<<Object instance>>);`

`db.save(user);`

## Update ##

`db.update(<<Object instance>>);`

# if authentication required #

`db.update(<<Object instance>>, <<String username>>, <<String password>>);`

if @AuthRequired and not provided throws an exception. Does not allow ID fields to be updated.

## Get ##

# Get on id #

`User user = db.get(new User().setUsername("TomDHarry"));`

# Get on other field #

`Airport origin = db.get(new Airport().setName("Newcastle"), "name");`

# Get list #

`List<Airport> canada = db.getList(new Airport().setCountry("CAN"), "country");`

## Find ##

# find one #

```
DBQuery query = DBQuery.and(
        DBQuery.and(DBQuery.condition("id.flightNumber", DBCondition.Relation.EQ, f.getFlightNumber()),
                DBQuery.condition("id.leg", DBCondition.Relation.EQ, f.getFlightType())),
        DBQuery.condition("id.startDate", DBCondition.Relation.LTE, f.getDeparture())
);
Price price = db.find(Price.class, query);
```

# find list #

//todo fill this in.

Find also allows for ordering/limiting/offsetting/paging.

## Exist ##

`boolean exists = db.exist(new User().setUsername("TomDHarry"));`

## Delete ##